import threading
from tkinter import Button

import json
from tkinter.ttk import Entry, Label

from util.comm.AsyncTask import AsyncTask
from util.comm.cmd_util import  wrap_with_try_except
from util.os.os_util import close_port_occupation
from util.window.config_window.config_button import create_config_button
from util.comm.path import getConfigPath
from util.comm.msg_util import msg
import tkinter as tk


class WinPyParamTool():
    def __init__(self):
        super().__init__()

    def frame(self, tab):
        self.tab = tab

        self.elements = []

        self.config_path = getConfigPath()
        self.button = create_config_button(self, True)

        # 打开文件并读取JSON数据
        with open(self.config_path, 'r', encoding="utf-8") as file:
            data = json.load(file)
        if isinstance(data, list):
            for index, c in enumerate(data):
                if 'is_active' in c:
                    if c['is_active'] == 0:
                        continue
                # 增加二级目录
                self.menu = self.create_tab(self.tab, c['menu'])
                self.elements.append(self.menu)
                content = c['content']
                params = content["params"]
                # 增加输入框
                map = {}
                if isinstance(params, list):
                    row = 1
                    for index, param in enumerate(params):
                        title = param['field_title']
                        field = param['field']

                        # 标题
                        self.label = Label(self.menu, text=title)
                        self.label.grid(row=row, column=0, padx=5, pady=5)

                        self.Entry = Entry(self.menu, width=36)
                        self.Entry.grid(row=row, column=1, padx=5, pady=5)

                        map[field] = self.Entry

                        row += 1

                        new_row = tk.Frame(self.menu)
                        new_row.grid(row=row, column=0, sticky='news')

                # 增加按钮
                cmd = content["cmd"]
                button = content["button"]
                self.button = Button(self.menu, text=button, command=lambda n=c['menu'],c=cmd,m=map: self.print_entry_sync(n,c,m), width=12)
                self.button.grid(row=row, column=1, padx=36, pady=5)

        else:
            print(data)

    def print_entry_sync(self, n, c, m):
        flag = "sync_flag_%s" % n
        if hasattr(self, flag) and getattr(self, flag) == '1':
            msg('正在处理,请勿重复操作')
            return
        setattr(self, flag, '1')
        wrap_print_entry = wrap_with_try_except(self.print_entry)
        AsyncTask(threading.Thread(target=wrap_print_entry, args=(c,m,)), lambda n=n: self.reset_sync(n)).execute_async()

    def reset_sync(self, n):
        flag = "sync_flag_%s" % n
        setattr(self, flag, '0')

    def print_entry(self, cmd, m):
        if cmd == 'close_port_occupation':
            port = m['port'].get()
            port = int(port)
            close_port_occupation(port)
        else:
            msg("指令【%s】不存在" % cmd)

    def create_tab(self, master_tabs, title):
        # 创建第一级tab t1
        tree_tab = tk.Frame(master_tabs)
        tree_tab.pack()

        master_tabs.add(tree_tab, text=title)
        return tree_tab
