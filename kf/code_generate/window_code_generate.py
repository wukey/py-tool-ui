import json
from tkinter import ttk

from util.comm.path import getConfigPath
from util.window import win_jar_run
from util.window.config_window.config_button import create_config_button


class WinCodeGenerate():
    def __init__(self):
        super().__init__()

    def frame(self, tab):
        self.tab = tab

        self.elements = []

        self.config_path = getConfigPath()
        self.button = create_config_button(self, True)

        self.menu = ttk.Frame(self.tab)
        self.menu.pack()
        self.elements.append(self.menu)

        config_path = getConfigPath()
        with open(config_path, 'r', encoding='utf-8') as f:
            config = json.load(f)
        win_jar_run.WinJavaRunTool().menuFrame(self.menu, config)