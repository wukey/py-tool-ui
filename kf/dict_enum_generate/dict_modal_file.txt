import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @@desc@@
 * @author wukai
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum @@file@@Enum {

@@field@@
    ;

    /**
     * 数据
     */
    private String data;

    /**
     * 描述
     */
    private String desc;

	public static String getDesc(String data) {
        for (@@file@@Enum e : @@file@@Enum.values()) {
            if(e.getData().equals(data)){
                return e.getDesc();
            }
        }
        return data;
    }

}
