# py_tool工具

## 目录
1. 产品背景
2. 产品安装
3. 功能介绍

### 1. 产品背景
在项目开发运维过程中，开发过程往往有些重复的代码需要反复编写，运维常见的问题补单子等等的重复操作，还有一些工作上常用的清理缓存以及请求转换的等等重复繁琐的问题，所以开发了一款可以替代这些工作的工具，尽可能通过修改配置来涵盖这些不断的重复工作。

### 2. 产品安装
程序仅支持windows，安装链接[releases](https://gitee.com/wukey/py-tool-ui/releases/tag/v0.0.1-beta)

### 3. 功能介绍

#### 3.1. 开发工具

##### 3.1.1. 清缓存

用于云仓清理ds调用缓存，采用两种方式，一种为url请求，一种为调用redis-cli方法。

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以满足不同场景的redis缓存清理情况，修改后保存关闭配置文件，工具会根据配置文件自动更新。

配置文件
```
    "name": "云仓线上", // 配置名称，按钮名称
    "mode": 0, // 模式，1为url请求，2为redis-cli
    "url": "", // url请求，例如http://127.0.0.1:8080/api/v1/cache/clear，多个逗号分隔
    "host": "", // redis-cli host为连接，例如127.0.0.1:6379，多个逗号分隔
    "key": "" //  redis-cli key为连接，例如cache:user:1234567890，多个逗号分隔
```

![img.png](md/1.png)

##### 3.1.2. ds解析

用于云仓解析ds，支持云仓的数据网关、模态框请求类参数自动解析参数还原sql。

![img.png](md/2.png)

##### 3.1.3. enum工具

用于查询数据库枚举字段，结果直接生成java中的枚举类

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以满足不同场景的枚举生成情况，修改后保存关闭配置文件，工具会根据配置文件自动更新。

配置文件
```
    "name": "wms测试库", // 配置名称，按钮名称
    "type": "oracle", // 数据库类型，目前支持oracle和mysql
    "host": "", // 数据库host
    "port": 1521, // 数据库端口,数字类型
    "service_name": "", // 库名
    "user": "", // 用户名
    "password": "", // 密码
    // sql语句，%s为字段名称，返回值必须为value_data和value_desc，否则无法解析
    "sql": "SELECT t.value_data as value_data,t.value_desc as value_desc FROM SYS_FIELD_DTL t where t.FIELD_NAME = '%s'"
```

![img.png](md/3.png)

![img.png](md/4.png)

##### 3.1.3. 代码生成

用于直接生成java中的类，开源代码生成地址：[https://github.com/davidfantasy/mybatis-plus-generator-ui.git](https://github.com/davidfantasy/mybatis-plus-generator-ui.git)

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以满足不同场景的启动jar包情况，修改后保存关闭配置文件，工具会根据配置文件自动更新。

配置文件
```
  {
  "jar_name": "mybatis-plus-generator-ui-0.0.1-self.jar",   // jar包名称
  "url": "http://127.0.0.1:8068",                           // 自动打开web地址
  "configs": [
              {
                "name": "wms-hz测试库",                    // 预配置环境名称
                "params": [                               // jar包启动参数，参数输入框
                  {
                    "field": "driverClassName",                 // 参数名
                    "value": "oracle.jdbc.driver.OracleDriver", // 参数值
                    "name": "驱动"                               // 参数名称
                  },
                  {
                    "field": "jdbcUrl",
                    "value": "jdbc:oracle:thin:@10.3.87.68:1521/wms",
                    "name": "链接"
                  },
                  ...
              ]
  }
```

![img.png](md/11.png)

#### 3.2. 运维工具

##### 3.2.1. 生成回单

接口调用回单生成工具。

![img.png](md/5.png)

##### 3.2.2. 运单宽表

接口调用记录宽表生成工具。

![img.png](md/6.png)

##### 3.2.3. 值班

接口调用记录值班工具。

![img.png](md/7.png)

#### 3.3. web工具

用于工具web服务的展示。

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以实现不同目录分类场景，修改后保存关闭配置文件，工具会根据配置文件自动更新。

```
  "menu": "工具箱",    // 1级菜单名称
  "content": [          // 菜单内容
            {
              "name": "sql工具",                  // 2级按钮名称
              "url": "https://tool.lu/sql/"        // 2级按钮跳转链接
            }
          ]
```

![img.png](md/8.png)

#### 3.4. 快捷启动

用于工具快捷方式的展示。

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以实现不同目录分类场景，修改后保存关闭配置文件，工具会根据配置文件自动更新。

```
  "menu": "开发工具",    // 1级菜单名称
  "content": [          // 菜单内容
            {
              "name": "notepad++",                  // 2级按钮名称
              "path": "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Notepad++"        // 2级按钮的快捷方式启动路径
            }
          ]
```

![img.png](md/9.png)

#### 3.5. 参数启动

用于工具快捷方式的展示。

可以点击`⚙`按钮进行配置，配置文件为`config.json`，可以实现不同目录分类场景，修改后保存关闭配置文件，工具会根据配置文件自动更新。

```
[
  {
  "menu": "弹窗提示",     // 菜单名称
  "content": {
              "params": [
                          {
                            "field_title": "提示信息",        // 参数输入框标题
                            "field": "message"              // 参数字段名称，用于替换cmd命令中的参数
                          },{
                            "field_title": "提示信息1",
                            "field": "message1"
                          }
                        ],
              "exe_file": "msg.exe",                        // 执行文件名称，必须和配置文件config.json同目录[exec_param_tool]下
              "cmd": "{exe} * \"{message}+{message1}\"",    // 执行命令，{exe}为执行文件名称程序会自动替换，{message}为参数字段名称，{message1}为参数字段名称
              "button": "提示"                               // 按钮名称，按钮按照执行cmd命令
              }
  }
]
```

![img.png](md/10.png)
