import threading
import tkinter as tk

class Scroller:
    def __init__(self, title, process):
        self.process = process

        root = tk.Tk()
        root.title(title)

        self.scrollbar = tk.Scrollbar(root)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.text_box = tk.Text(root, yscrollcommand=self.scrollbar.set)
        self.text_box.pack(side=tk.LEFT, fill=tk.BOTH)

        self.scrollbar.config(command=self.text_box.yview)

        root.mainloop()

    # 获取当前对象
    def get_self(self):
        return self

    def print_to_scroll(self, output):
        self.text_box.insert(tk.END, output + '\n')
        self.text_box.see(tk.END)

    # 销毁
    def destroy(self):
        self.process.terminate()
