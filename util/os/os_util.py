import os
import signal

from util.comm.msg_util import msg

def close_port_occupation(port):
    try:
        with os.popen(f'netstat -aon|findstr "{port}"') as res:
            res = res.read().split('\n')
            result = []
            for line in res:
                temp = [i for i in line.split(' ') if i != '']
                if len(temp) > 4:
                    result.append(temp[4])
        for i in result:
            os.kill(int(i), signal.SIGINT)
            msg(f"杀死占用端口的进程成功，该进程pid：{i}")
            return
        msg(f"没有与端口 {port} 绑定的进程")
    except Exception as e:
        msg(f"没有与端口 {port} 绑定的进程")
