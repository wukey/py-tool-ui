import subprocess

def run_cmd(cmd_str: str):
    return subprocess.Popen(cmd_str,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            text=True).communicate()[0]
