from util.comm.msg_util import msg
from util.database.mysql import MyMysql
from util.database.oracle import MyOracle

def query_sql(sql, config):
    database_type = config['type']
    if database_type == 'oracle':
        field_list = MyOracle().searchObj(config, sql)
    elif database_type == 'mysql':
        field_list = MyMysql().searchObj(config, sql)
    else:
        msg('数据库类型【%s】配置错误' % database_type)
        return []
    if field_list:
        if field_list == "0":
            return []
        return field_list
    else:
        return []

def query_1p(sql, field, config):
    database_type = config['type']
    if database_type == 'oracle':
        field_list = MyOracle().search(config, sql % field)
    elif database_type == 'mysql':
        field_list = MyMysql().search(config, sql % field)
    else:
        msg('数据库类型【%s】配置错误' % database_type)
        return None
    if field_list:
        if field_list == "0":
            return "字段【%s】不存在" % field
        return field_list
    else:
        return "字段【%s】不存在" % field

def list_to_str(field_list, field):
    string_str = ""
    if isinstance(field_list, list):
        # 判断数组长度是否为1
        if field_list.__len__() == 1:
            filed_str = field_list[0][0] + ",<br/>"
            string_str = f'|【{field}】|' + "|".join(str(item) for item in field_list[0] if item is not None) + "|\\n"
            return filed_str, string_str
        for index, row in enumerate(field_list):
            filed_str = "#." + field_list[0][0] + ",<br/>"
            if index == 0:
                string_str += f'|【{field}】|' + "|".join(str(item) for item in row if item is not None) + "|\\n"
            else:
                string_str += "|  |" + "|".join(str(item) for item in row if item is not None) + "|\\n"
        return filed_str, string_str
    else:
        filed_str = "?." + field + ",<br/>"
        string_str += f'|【{field}】|' + field_list + "|   |   |   |   |\\n"
        return filed_str, string_str
