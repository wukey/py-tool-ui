import inspect
import os


def setExec(path):
    global exec_path
    exec_path = path

def setCwd(path):
    global cwd_path
    cwd_path = path

def getExec():
    return exec_path

def getCwd():
    return cwd_path

def printPath():
    print("exec: " + exec_path)
    print("cwd:  " + cwd_path)

def getPath():
    # 获取当前方法的堆栈信息
    frame = inspect.currentframe()
    # 获取调用方法的堆栈信息
    calling_frame = inspect.getouterframes(frame)[1]
    # 获取模块路径
    module_path = calling_frame.filename
    # 去掉文件名
    dir_path = os.path.dirname(module_path)
    # 获取文件路径的最后一部分
    path = os.path.relpath(dir_path, exec_path)

    project_path = cwd_path

    directory = os.path.join(project_path, path)
    return directory

# 调用端当前路径-配置文件
def getConfigPath():
    # 获取当前方法的堆栈信息
    frame = inspect.currentframe()
    # 获取调用方法的堆栈信息
    calling_frame = inspect.getouterframes(frame)[1]
    # 获取模块路径
    module_path = calling_frame.filename
    # 去掉文件名
    dir_path = os.path.dirname(module_path)
    # 获取文件路径的最后一部分
    path = os.path.relpath(dir_path, exec_path)

    project_path = cwd_path

    directory = os.path.join(project_path, path)

    config_path = os.path.join(directory, 'config.json')
    return config_path