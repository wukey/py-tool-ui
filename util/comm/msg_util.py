import os

from util.comm.path import getPath


def msg(message):
    project_path = getPath()
    msg = os.path.join(project_path, 'msg.exe')
    message = handMsg(message)
    os.popen(f"{msg} * \"{message}\"")

def handMsg(message):
    if len(message) > 250:
        message = message[:120] + '...' + message[-120:-1]
    return message