import json
from datetime import datetime, date


class LoadDatetime(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)

def ensure_utf8(obj):
    if isinstance(obj, str):
        return obj.encode('utf-8').decode('utf-8')
    elif isinstance(obj, dict):
        return {k: ensure_utf8(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [ensure_utf8(item) for item in obj]
    else:
        return obj