from util.window import win_http_param
from util.comm.path import getConfigPath


class WinYwHttp():
    def __init__(self):
        super().__init__()

    def frame(self, tab):
        config_path = getConfigPath()
        win_http_param.WinHttpParamTool().tabFrame(tab, config_path)