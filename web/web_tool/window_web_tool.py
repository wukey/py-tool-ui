import threading
import webbrowser
from tkinter import Button

import json

from util.comm.AsyncTask import AsyncTask
from util.window.config_window.config_button import create_config_button
from util.comm.path import getConfigPath
from util.comm.msg_util import msg
import tkinter as tk


class WinWebTool():
    def __init__(self):
        super().__init__()

    def frame(self, tab):
        self.tab = tab

        self.elements = []

        self.config_path = getConfigPath()
        self.button = create_config_button(self, True)

        # 打开文件并读取JSON数据
        with open(self.config_path, 'r', encoding="utf-8") as file:
            data = json.load(file)
        if isinstance(data, list):
            for index, c in enumerate(data):
                # 增加二级目录
                if 'is_active' in c:
                    if c['is_active'] == 0:
                        continue
                self.menu = self.create_tab(self.tab, c['menu'])
                self.elements.append(self.menu)
                content = c['content']
                if isinstance(content, list):
                    row = 1
                    column = 0
                    for index, conf in enumerate(content):
                        # 增加三级内容
                        if 'is_active' in conf:
                            if conf['is_active'] == 0:
                                continue
                        name = conf['name']
                        url = conf['url']
                        self.button = Button(self.menu, text=name, command=lambda u=url: self.print_entry_sync(u), width=12)
                        self.button.grid(row=row, column=column, padx=5, pady=5)
                        column += 1
                        if column % 4 == 0:  # 每四个按钮换行
                            column = 0
                            row += 1
                            new_row = tk.Frame(self.menu)
                            new_row.grid(row=row, column=column, sticky='news')
                        # self.button.pack(side='left', fill=None, expand=False, anchor='ne', padx=20, pady=30)

        else:
            print(data)

    def print_entry_sync(self, config):
        if hasattr(self, 'sync_flag') and self.sync_flag == '1':
            msg('正在处理,请勿重复操作')
            return
        self.sync_flag = '1'
        AsyncTask(threading.Thread(target=self.print_entry, args=(config,)), lambda: self.reset_sync()).execute_async()

    def reset_sync(self):
        self.sync_flag = '0'

    def print_entry(self, config):
        webbrowser.open(config)

    def create_tab(self, master_tabs, title):
        # 创建第一级tab t1
        tree_tab = tk.Frame(master_tabs)
        tree_tab.pack()

        master_tabs.add(tree_tab, text=title)
        return tree_tab
