import json
import os
import tkinter as tk
from tkinter import ttk
from kf.clear_redis import window_clear_redis
from kf.code_generate import window_code_generate
from kf.create_class import window_create_class
from kf.create_sql import window_create_sql
from kf.dict_enum_generate import window_dict_enum
from kf.ds_parse import window_ds_parse
from exec.exec_tool import window_exec_tool
from layout.stream_db import window_stream_db
from util.comm.msg_util import msg
from util.comm.path import setExec, setCwd, printPath, getConfigPath
from web.web_param_tool import window_http_param_tool
from yw import window_yw_http
from web.web_tool import window_web_tool
from exec.exec_param_tool import window_exec_param_tool
from exec.py_param_tool import window_py_param_tool
from yw.data_handle_pc.jzt_jcpt import window_pc_jcpt
from yw.data_handle_pc.jzt_yclc import window_pc_yclc
from yw.data_handle_pc.nacos import window_pc_nacos
from yw.data_sql_assemble import window_yw_sql_data


class WindowRoot(tk.Tk):
    def __init__(self):
        super().__init__()

        # 设置窗口标题
        self.title("外挂程序-v0.0.1")
        # 设置窗口大小，例如宽度 300，高度 200
        self.geometry("500x358")

        # # 创建Tab控件
        master_tabs = ttk.Notebook(self)
        master_tabs.pack(fill=tk.BOTH, expand=1)

        self.config_path = getConfigPath()
        with open(self.config_path, 'r', encoding="utf-8") as file:
            configs = json.load(file)

        if isinstance(configs, list):
            for index, c in enumerate(configs):
                id = c["id"]
                if id in root_config:
                    cs = c["children"]
                    if c["is_active"] == 1:
                        tab = configTab(master_tabs, c["name"], c["has_tabs"], root_config.get(id))
                        if isinstance(cs, list):
                            for i, cc in enumerate(cs):
                                c_id = cc["id"]
                                if c_id in root_config:
                                    if cc["is_active"] == 1:
                                        configTab(tab, cc["name"], cc["has_tabs"], root_config.get(c_id))
        else:
            msg("root_config配置格式有误")


def create_tab(master_tabs, title):
    # 创建第一级tab t1
    tree_tab = ttk.Frame(master_tabs)
    tree_tab.pack()

    master_tabs.add(tree_tab, text=title)
    return tree_tab


def create_tabs(master_tabs, title):
    # 创建第一级tab t1
    tree_tab = ttk.Frame(master_tabs)
    tree_tab.pack()

    tree_tabs = ttk.Notebook(tree_tab)
    tree_tabs.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    master_tabs.add(tree_tab, text=title, compound=tk.TOP)
    return tree_tabs


def configTab(tabs, name, has_tabs, winObj=None):
    if has_tabs == 1:
        tab = create_tabs(tabs, name)
    else:
        tab = create_tab(tabs, name)
    if None != winObj:
        winObj.frame(tab)
    return tab


root_config = {
    1: None,
    101: window_clear_redis.ClsRedis(),
    102: window_ds_parse.WinDsParse(),
    103: window_code_generate.WinCodeGenerate(),
    104: window_dict_enum.Enums(),
    105: window_create_class.ClassInit(),
    106: window_create_sql.SqlInit(),
    2: None,
    201: window_yw_http.WinYwHttp(),
    202: window_yw_sql_data.WinYwSqlData(),
    203: window_pc_nacos.WinPcNacos(),
    204: window_pc_jcpt.WinPcJcpt(),
    205: window_pc_yclc.WinPcYclc(),
    3: None,
    301: window_web_tool.WinWebTool(),
    302: window_http_param_tool.WinHttpParamTool(),
    303: window_exec_tool.WinExecTool(),
    304: window_exec_param_tool.WinExecParamTool(),
    4: None,
    401: window_py_param_tool.WinPyParamTool(),
    5: None,
    501: window_stream_db.Win(),
}


if __name__ == '__main__':
    setCwd(os.getcwd())
    setExec(os.path.dirname(os.path.realpath(__file__)))

    printPath()
    window_root = WindowRoot()
    # 运行主窗体
    window_root.mainloop()
